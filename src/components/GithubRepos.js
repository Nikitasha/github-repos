import React, { useState } from "react"
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import './GithubRepos.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const GithubRepos = () => {
  const [users, setUsers] = useState([])
  const [searchInput, setSearchInput] = useState('');
  const [errorOccur, setErrorOccur] = useState();
  const [forked, setForked] = useState(false);


  const handleChange = (e) => {
    setSearchInput(e.target.value);
  };

  const handleFork = (e) => {
    if (e.target.checked) {
      setForked(true);

    }
    else {
      setForked(false);
    }
  }

  const fetchData = async () => {
    const res = await fetch(`https://api.github.com/users/${searchInput}/repos`)
    const data = await res.json();
    console.log(data);

    if (res.ok) {
      if (data.length === 0) {
        setUsers([]);
        setErrorOccur(`${searchInput} Github user has not created any repository`)
        console.log("No Repos found")
      }
      else {
        data.sort(function (a, b) { return b.size - a.size });
        setUsers(data);

      }
    }
    else if (res.status === 404) {
      setErrorOccur(data.message);
    }
  }


  return (
    <div style={{ alignItems: 'center', margin: '20px 160px' }}>
      <input type="text" placeholder="Github User Name" value={searchInput} onChange={handleChange} />
      <label for="vehicle1" style={{ marginInline: '20px' }}> Include Folks: </label>

      <input type="checkbox" name="forks" value={forked} onChange={handleFork} />

      <Button onClick={fetchData} disabled={searchInput.length === 0 ? "disabled" : ""} style={{ marginInline: '20px' }}>Submit</Button>

      <Table responsive bordered hover variant="stripped">
        <thead>
          <tr>
            <th>Repo Name</th>
            <th>Description</th>
            <th>Language</th>
            <th>Size</th>

          </tr>
        </thead>
        <tbody>

          {errorOccur && (
            <div>{`There is a problem fetching the data - ${errorOccur}`}</div>
          )}

          {users &&
            users.map((user) => (
              <tr key={user.id} >
                <td> {user.name}</td>
                <td>{user.description}</td>
                <td>{user.language}</td>
                <td>{user.size}</td>

              </tr>
            ))}
        </tbody>
      </Table>
    </div>

  )
}

export default GithubRepos;